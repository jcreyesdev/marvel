import UIKit

class CharacterComicTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var titleComicLabel: UILabel!
    @IBOutlet weak var dateComicLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
