import UIKit


class CharacterDetailModule {
    
    static func build(character: Character) -> UIViewController {
        let view = CharacterDetailView(character: character)
        let interactor = CharacterDetailInteractor()
        let router = CharacterDetailRouter()
        let presenter = CharacterDetailPresenter()
        
        
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        
        view.presenter = presenter
        
        interactor.presenter = presenter
        
        router.viewController = view
        
        return view
    }
    
}
