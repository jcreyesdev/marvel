import Foundation


struct EventDataWrapper: Codable {
    var code: Int?
    var status: String?
//    var copyright: String?
//    var attributionText: String?
//    var attributionHTML: String?
    var data: EventDataContainer?
//    var etag: String?
}

struct EventDataContainer: Codable {
    var offset: Int?
    var limit: Int?
    var total: Int?
    var count: Int?
    var results: [Event]?
}

struct Event: Codable {
//    var id: Int?
    var title: String?
    var description: String?
//    var resourceURI: String?
//    var urls: [UrlEvent]?
//    var modified: String?
    var start: String?
    var end: String?
    var thumbnail: ImageEvent?
    var comics: ComicListEvent?
//    var stories: StoryListEvent?
//    var series: SeriesListEvent?
//    var characters: CharacterListEvent?
//    var creators: CreatorListEvent?
//    var next: EventSummaryEvent?
//    var previous: EventSummaryEvent?
}

//struct UrlEvent: Codable {
//    var type: String?
//    var url: String?
//}

struct ImageEvent: Codable {
    var path: String?
    var ext: String?
    
    enum CodingKeys: String, CodingKey {
        case path
        case ext = "extension"
    }
}

struct ComicListEvent: Codable {
    var available: Int?
    var returned: Int?
    var collectionURI: String?
    var items: [ComicSummaryEvent]?
}

struct ComicSummaryEvent: Codable {
    var resourceURI: String?
    var name: String?
}

//struct StoryListEvent: Codable {
//    var available: Int?
//    var returned: Int?
//    var collectionURI: String?
//    var items: [StorySummaryEvent]?
//}

//struct StorySummaryEvent: Codable {
//    var resourceURI: String?
//    var name: String?
//    var type: String?
//}

//struct SeriesListEvent: Codable {
//    var available: Int?
//    var returned: Int?
//    var collectionURI: String?
//    var items: [SeriesSummaryEvent]?
//}

//struct SeriesSummaryEvent: Codable {
//    var resourceURI: String?
//    var name: String?
//}

//struct CharacterListEvent: Codable {
//    var available: Int?
//    var returned: Int?
//    var collectionURI: String?
//    var items: [CharacterSummaryEvent]?
//}

//struct CharacterSummaryEvent: Codable {
//    var resourceURI: String?
//    var name: String?
//    var role: String?
//}

//struct CreatorListEvent: Codable {
//    var available: Int?
//    var returned: Int?
//    var collectionURI: String?
//    var items: [CreatorSummaryEvent]?
//}

//struct CreatorSummaryEvent: Codable {
//    var resourceURI: String?
//    var name: String?
//    var role: String?
//}

//struct EventSummaryEvent: Codable {
//    var resourceURI: String?
//    var name: String?
//}
