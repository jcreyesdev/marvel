import Foundation
import UIKit


protocol EventsRouterProtocol: AnyObject {
    func goToEventDetail(selection: Event)
}

protocol EventsPresenterProtocol: AnyObject {
    func numberOfRowsInSection() -> Int
    func cellForRowAt(indexPath: IndexPath, tableView: UITableView) -> UITableViewCell
    func didSelected(indexPath: IndexPath)
    func getEventList()
    func getEventListSuccess(result: EventDataWrapper)
    func getEventListFailure()
}


protocol EventsInteractorProtocol: AnyObject {
    var presenter: EventsPresenterProtocol?  { get set }
    
    func invokeEventsServiceWith(param: String)
}


protocol EventsViewProtocol: AnyObject {
    var presenter: EventsPresenterProtocol?  { get set }
    
    func reloadDataTableView()
    func showLoading()
    func hideLoading()
}
