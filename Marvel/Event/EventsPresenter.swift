import UIKit


class EventsPresenter: EventsPresenterProtocol {
    
    
    weak var view: EventsViewProtocol?
    var interactor: EventsInteractorProtocol?
    var router: EventsRouterProtocol?
    
    var eventsList: [Event] = []
    
    
    func numberOfRowsInSection() -> Int {
        return eventsList.count
    }
    
    func cellForRowAt(indexPath: IndexPath, tableView: UITableView) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell") as! EventTableViewCell
        let event = eventsList[indexPath.row]
        cell.cellView.layer.cornerRadius = 4
        cell.cellView.clipsToBounds = true
        
        if let path = event.thumbnail?.path, let ext = event.thumbnail?.ext {
            let link = path.replacingOccurrences(of: "http://", with: "https://")
            let urlString = "\(link).\(ext)"
            cell.eventImage.kf.indicatorType = .activity
            cell.eventImage.kf.setImage(with: URL(string: urlString), placeholder: UIImage(named: "placeholder"), options: [.transition(.fade(0.5))])
        }
        
        cell.titleEventLabel.text = event.title
        cell.descriptionEventLabel.text = event.start
        
        return cell
    }
    
    func didSelected(indexPath: IndexPath) {
        let event = eventsList[indexPath.row]
        router?.goToEventDetail(selection: event)
    }
    
    
    // Service events
    func getEventList() {
        self.view?.showLoading()
        self.interactor?.invokeEventsServiceWith(param: "orderBy=startDate&limit=25&")
    }
    
    func getEventListSuccess(result: EventDataWrapper) {
        if let events = result.data?.results {
            self.eventsList.append(contentsOf: events)
            self.view?.reloadDataTableView()
            self.view?.hideLoading()
        }
        
    }
    
    func getEventListFailure() {
        self.view?.hideLoading()
    }
}
