import UIKit


class EventsView: UIViewController, EventsViewProtocol {
    
	
    var presenter: EventsPresenterProtocol?
    
    @IBOutlet weak var eventsTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Events"
        self.navigationController?.navigationBar.barStyle = .black
        self.registerCell(tableView: eventsTableView)
        self.setUpTableView()
        self.presenter?.getEventList()
    }
    
    func registerCell(tableView: UITableView) {
        let identifier = "EventTableViewCell"
        tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
    }
    
    func setUpTableView() {
        eventsTableView.delegate = self
        eventsTableView.dataSource = self
        eventsTableView.estimatedRowHeight = 120.0
    }
    
    func reloadDataTableView() {
        eventsTableView.reloadData()
    }
    
    func showLoading() {
        self.showSpinner()
    }
    
    func hideLoading() {
        self.hideSpinner()
    }
    
}


// MARK: - UITableViewDelegate & UITableViewDataSource
extension EventsView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.didSelected(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter?.numberOfRowsInSection() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return presenter?.cellForRowAt(indexPath: indexPath, tableView: tableView) ?? UITableViewCell()
    }
    
}
