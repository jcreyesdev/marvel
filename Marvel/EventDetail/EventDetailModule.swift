import UIKit


class EventDetailModule {
    
    static func build(event: Event) -> UIViewController {
        let view = EventDetailView(event: event)
        let interactor = EventDetailInteractor()
        let router = EventDetailRouter()
        let presenter = EventDetailPresenter()
        
        
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        
        view.presenter = presenter
        
        interactor.presenter = presenter
        
        router.viewController = view
        
        return view
    }
    
}
