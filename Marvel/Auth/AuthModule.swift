import UIKit


class AuthModule {
    
    static func build() -> UIViewController {        
        let view = AuthView()
        let interactor = AuthInteractor()
        let router = AuthRouter()
        let presenter = AuthPresenter()
        
        
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        
        view.presenter = presenter
        
        interactor.presenter = presenter
        
        router.viewController = view
        
        return view
    }
    
}
