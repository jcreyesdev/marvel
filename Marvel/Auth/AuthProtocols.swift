import Foundation


protocol AuthRouterProtocol: AnyObject {
    func goToHomeView(email: String, provider: ProviderType, animated: Bool)
}

protocol AuthPresenterProtocol: AnyObject {
    func onPressedSignUpButton(email: String, password: String, provider: ProviderType)
    func onPressedLogInButton(email: String, password: String, provider: ProviderType)
    func isAuth()
    func goToHomeView(email: String, provider: ProviderType, animated: Bool)
}


protocol AuthInteractorProtocol: AnyObject {
    var presenter: AuthPresenterProtocol?  { get set }
}


protocol AuthViewProtocol: AnyObject {
    var presenter: AuthPresenterProtocol?  { get set }
    
    func showToast(message: String)
}
