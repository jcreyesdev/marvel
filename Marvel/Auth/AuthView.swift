import UIKit
import FirebaseAuth


class AuthView: UIViewController, AuthViewProtocol, UITextFieldDelegate {
    
	
    var presenter: AuthPresenterProtocol?
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var logInButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.isAuth()
        self.setUpTextField()
        self.setUpLayout()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.view.endEditing(true)
    }
    
    func isAuth() {
        presenter?.isAuth()
    }
    
    func setUpTextField() {
        self.emailTextField.delegate = self
        self.passwordTextField.delegate = self
        
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "#00000061")!])
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "#00000061")!])
    }
    
    func setUpLayout() {
        self.signUpButton.layer.cornerRadius = 4
        self.signUpButton.clipsToBounds = true
        self.logInButton.layer.cornerRadius = 4
        self.logInButton.clipsToBounds = true
    }
    
    @IBAction func signUpButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if let email = emailTextField.text, let password = passwordTextField.text {
            self.presenter?.onPressedSignUpButton(email: email, password: password, provider: .basic)
        }
    }
    
    @IBAction func logInButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if let email = emailTextField.text, let password = passwordTextField.text {
            self.presenter?.onPressedLogInButton(email: email, password: password, provider: .basic)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func showToast(message: String) {
        self.showToastWith(message: message)
    }
    
}
