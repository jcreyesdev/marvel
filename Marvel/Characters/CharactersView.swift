import UIKit


class CharactersView: UIViewController, CharactersViewProtocol {
    
	
    var presenter: CharactersPresenterProtocol?
    
    @IBOutlet weak var charactersTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Characters"
        self.navigationController?.navigationBar.barStyle = .black
        self.registerCell(tableView: charactersTableView)
        self.presenter?.getTableView(tableView: charactersTableView)
        self.setUpTableView()
        self.charactersTableView.isHidden = false
        self.presenter?.getCharacterList()
    }
    
    func registerCell(tableView: UITableView) {
        let identifier = "CharacterTableViewCell"
        tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
    }
    
    func setUpTableView() {
        charactersTableView.delegate = self
        charactersTableView.dataSource = self
        charactersTableView.estimatedRowHeight = 120.0
    }
    
    func reloadDataTableView() {
        charactersTableView.reloadData()
    }
    
    func showLoading() {
        self.showSpinner()
    }
    
    func hideLoading() {
        self.hideSpinner()
    }
    
}


// MARK: - UITableViewDelegate & UITableViewDataSource
extension CharactersView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.didSelected(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter?.numberOfRowsInSection() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return presenter?.cellForRowAt(indexPath: indexPath, tableView: tableView) ?? UITableViewCell()
    }
}


// MARK: - UIScrollViewDelegate
extension CharactersView: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.presenter?.scrollViewDidScroll(scrollView)
    }
    
}
