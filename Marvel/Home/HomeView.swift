import UIKit


enum ProviderType: String {
    case basic
}


class HomeView: UIViewController, HomeViewProtocol {
    
    
    var presenter: HomePresenterProtocol?
    
    var email: String
    var provider: ProviderType
    private let url = "https://gateway.marvel.com:443/v1/public/characters?limit=15&offset=15&ts=1&apikey=4cb38e9a77dd01e2463bff9981683b93&hash=b6dade92c534cbc76dc69cc6004ed9b2"

    
    init(email: String, provider: ProviderType) {
        self.email = email
        self.provider = provider
        super.init(nibName: "HomeView", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barStyle = .black
        self.saveSession()
        self.createTabBar()
    }
    
    func saveSession() {
        let defaults = UserDefaults.standard
        defaults.set(email, forKey: "email")
        defaults.set(provider.rawValue, forKey: "provider")
        defaults.synchronize()
    }
    
    func removeSession() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "email")
        defaults.removeObject(forKey: "provider")
        defaults.synchronize()
    }
    
    func createTabBar() {
        let tabBarController = UITabBarController()
        
        let vc1 = charactersTabBar()
        let vc2 = eventsTabBar()
        
        tabBarController.setViewControllers([vc1, vc2], animated: true)
        tabBarController.modalPresentationStyle = .fullScreen
        self.navigationController?.present(tabBarController, animated: false)
    }
    
    func charactersTabBar() -> UIViewController {
        let tab = UINavigationController(rootViewController: CharactersModule.build())
        tab.navigationBar.topItem?.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "power"), style: .done, target: self, action: #selector(closeSessionButtonAction))
        tab.tabBarItem.image = UIImage(named: "characterDeselected")
        tab.tabBarItem.selectedImage = UIImage(named: "characterSelected")
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor(named: "#262626")!], for: .selected)
        tab.title = "Characters"
        
        return tab
    }
    
    func eventsTabBar() -> UIViewController {
        let tab = UINavigationController(rootViewController: EventsModule.build())
        tab.navigationBar.topItem?.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "power"), style: .done, target: self, action: #selector(closeSessionButtonAction))
        tab.tabBarItem.image = UIImage(named: "eventDeselected")
        tab.tabBarItem.selectedImage = UIImage(named: "eventSelected")
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor(named: "#262626")!], for: .selected)
        tab.title = "Events"
        
        return tab
    }
    
    @objc func closeSessionButtonAction() {
        self.removeSession()
        self.presenter?.onCloseSessionButtonAction()
    }
    
    func showToast(message: String) {
        self.showToastWith(message: message)
    }
    
}
