import UIKit
import Firebase


class HomePresenter: HomePresenterProtocol {
    
    
    weak var view: HomeViewProtocol?
    var interactor: HomeInteractorProtocol?
    var router: HomeRouterProtocol?
    
    
    func onCloseSessionButtonAction() {
        do {
            try Auth.auth().signOut()
            self.router?.goBackToAuth()
        } catch {
            let message = "There was an error closing the account, please try again."
            self.view?.showToast(message: message)
        }
    }
    
}
